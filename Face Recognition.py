import cv2
import pickle

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")

labels = {}
with open("labels.pickle", 'rb') as f:
    og_labels = pickle.load(f)
    labels = {v:k for k,v in og_labels.items()}

print("Streaming started")
video_capture = cv2.VideoCapture(0, cv2.CAP_DSHOW)

while True:

    ret, frame = video_capture.read()
    frame_flip = cv2.flip(frame,1)
    gray = cv2.cvtColor(frame_flip, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=4,
        flags= cv2.CASCADE_SCALE_IMAGE

    )



    for (x, y, w, h) in faces:
        id_, conf = recognizer.predict(gray[y:y+h, x:x+w])
        if conf>=45 and conf <= 85:
            name = labels[id_]
        else:
            name = "Unknown"
        cv2.rectangle(frame_flip, (x, y), (x+w, y+h), (0, 255, 0), 2)
        cv2.putText(frame_flip,name, (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)


    cv2.imshow('Stream', frame_flip)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        print("Streaming Stopped")
        break



video_capture.release()
cv2.destroyAllWindows()
