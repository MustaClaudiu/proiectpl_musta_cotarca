import os
import cv2
import numpy as np
from PIL import Image
import pickle


Base = os.path.dirname(os.path.abspath(__file__))
image = os.path.join(Base, "Imagini")

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()


y_name = []
x_train = []
id= 0
label_ids= {}

for root, dirs, files in os.walk(image):
    for file in files:
        if file.endswith("png") or file.endswith("jpg"):
            path = os.path.join(root, file)
            nameID = os.path.basename(root)
            #print(nameID,path)
            if not nameID in label_ids:
                label_ids[nameID] = id
                id = id +1
            id_ = label_ids[nameID]
            #print(label_ids)
            #x_train.append(path)
            #y_name.append(nameID)
            pil_image = Image.open(path).convert("L")
            size = (550,550)
            final_image = pil_image.resize(size, Image.ANTIALIAS)
            image_array = np.array(final_image,"uint8")
            #print(image_array)
            faces = faceCascade.detectMultiScale(image_array, scaleFactor=1.1, minNeighbors=4)

            for(x,y,w,h) in faces:
                roi = image_array[y:y+h, x:x+w]
                x_train.append(roi)
                y_name.append(id_)
#print(y_name)
#print(x_train)

with open("labels.pickle", 'wb') as f:
    pickle.dump(label_ids,f)

recognizer.train(x_train, np.array(y_name))
recognizer.save("trainer.yml")
print("The training is done")
